---
title: "Home"
---

# FITLab Reading Group
The FITLab Reading Group exists to create a safe, welcoming, and supportive place to bring together a diverse range of researchers (MRes & PhD students, RAs, ROs, and academics) interested in Human-Computer Interaction and collectively develop research skills.

## A potted history
This group was originally a collaborative space for one of our co-chair's research students to discuss research together. It has evolved into the form it takes today over the last few years, with much of this effort coming from Matt, Nicholas, and Peter.

## Research themes
There follows a list of dynamic themes that characterise the interests of the people in the group, they ebb and flow with membership (which is self-selecting). **Everyone** is invited and welcome to attend the group sessions! If you're in the group and don't see yourself represented, get in touch!

- Human-Centred Design
- Human-Centred AI Interaction
- Tangible User Interfaces
- Speech and Language Technology
- Emergent Users / Technology and Resource Constraints
- Human-Robot Interaction
- User Privacy and Security
- Health & Wellbeing
- Engineering & Smart Manufacturing
- Education
- Law & Terrorism
- Digital Economy
- Science and Technology Studies
- Sociology of Computing

## The method
We invite members to give a varied format of presentations, questions, discussions, and shared readings (+ review) during the hour. The material presented will be a mixture of, but not limited to:

- Practice talks for conferences
- Presentation & discussion of results
- Presentation & discussion of research design
- Presentation & discussion of a paper read (individually or as a group)
- Discussion of paper submission reviews

## Location and time
We meet on Thursday at 1-2pm (BST), usually in the Robert Recorde Room in the Computational Foundry at Swansea University's Bay Campus (changes to this will be announced ahead of time).

## What's on?
We maintain a list of upcoming sessions on [our events page]({{< relref "events/_index.html" >}}). We also send weekly updates over the CS-Fitlab mailing list, get in touch with Genevieve if you're not on the list! If you're interested in giving a talk, get in touch with the organisers at [fitlab-rg@swansea.ac.uk](mailto:fitlab-rg@swansea.ac.uk)!

## The organisers 
**Co-chairs:** [Matt Roach](mailto:m.j.roach@swansea.ac.uk) & [Nicholas Micallef](mailto:nicholas.micallef@swansea.ac.uk) (responsible for planning and chairing meetings)\
**Co-organisers:** [Genevieve Clifford](mailto:genevieve.clifford@swansea.ac.uk) & [Manjiri Joshi](mailto:m.g.joshi@swansea.ac.uk) (responsible for organising logistics, room booking & set-up, email reminders, keeping a diary of planned sessions and maintaining web presence)