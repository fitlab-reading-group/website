import { createApp } from 'vue'
import Airtable from 'airtable'
import SuggestionBox from '../vue/SuggestionBox.vue'
import Suggestions from '../vue/Suggestions.vue'

// Vue.js config
const app = createApp({})
app.component('SuggestionBox', SuggestionBox).component('Suggestions', Suggestions)
app.mount('#app')