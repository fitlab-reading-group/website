const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')

module.exports = { 
  entry: './assets/js/main.js',
  output: {
    path: path.resolve(__dirname, './static'),
    filename: 'build.js'
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: ['vue-style-loader', 'css-loader'],
          },
        },
      },
    ]
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm-browser.prod.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  plugins: [
    new VueLoaderPlugin()
  ],
}

if (process.env.NODE_ENV === 'production') {
  module.exports.mode = 'production'
} 