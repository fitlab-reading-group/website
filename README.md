# kasi-sin
This is a starting point for a static site that combines Hugo and Vue.js. If you'd like to use it, please fork it!

## Usage
Before starting, install both Hugo and Node / npm.

1. Set information about your website in `hugo.toml` (in the parent directory)
   1. You can optionally set the theme of the website by using the setting at the bottom of this list
2. Update the footer as you'd like in `layouts/partials/footer.html`
3. Add content in the `content` directory
   1. Any Vue.js assets should go in `assets/vue` and be registered in `assets/js/main.js`
   2. Data for the navbar should be placed in `data/nav.json` and uses the same format as in https://gitlab.com/mun-tonsi/kasi
4. Run a test build by running both `npm run watch` and `hugo server --disableFastRender --cleanDestinationDir`
5. When you're ready to deploy you can either:
   1. Build locally by running `npm run ci` and `hugo` (with no options) and push to a web server using a method you're familiar with
   2. Create a remote GitHub / GitLab repository and push to it; the CI/CD scripts in this repository should run automatically, you may have to tweak your GH/GL Pages settings to get this to work

### Theme
```toml
[params]
  colour = 'xyz'
```

Where `xyz` is a theme from https://gitlab.com/mun-tonsi/kule